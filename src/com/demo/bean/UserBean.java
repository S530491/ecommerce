package com.demo.bean;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import com.demo.model.User;

public class UserBean {
	Connection con;
	public UserBean(){
		try {
			Class.forName("oracle.jdbc.driver.OracleDriver");
			 con=DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:oracle","hr","hr");
		}catch(Exception e) {
			e.printStackTrace();
		}
	}
	public ArrayList<User> readAllUsers(){
		ArrayList<User> list=new ArrayList<User>();
		try {
			PreparedStatement ps=con.prepareStatement("select * from users");
			ResultSet rs=ps.executeQuery();
		while(rs.next()) {
			User user=new User();
			user.setId(rs.getInt(1));
			user.setName(rs.getString(2));
			user.setPassword(rs.getString(3));
			user.setAge(rs.getInt(4));
			list.add(user);
		}
		}catch(Exception e) {
			e.printStackTrace();
		}
		return list;
	}
	public ArrayList<User> deleteFromUsers(int id){
		ArrayList<User> list=new ArrayList<User>();
		try {
		PreparedStatement ps=con.prepareStatement("delete from users where id=?");
		ps.setInt(1, id);
		ps.executeUpdate();
		return readAllUsers();
		
//		con.prepareStatement("select * from users");
//		ResultSet rs=ps.executeQuery();
//	while(rs.next()) {
//		User user=new User();
//		user.setId(rs.getInt(1));
//		user.setName(rs.getString(2));
//		user.setPassword(rs.getString(3));
//		user.setAge(rs.getInt(4));
//		list.add(user);
//	}
	}catch(Exception e) {
		e.printStackTrace();
}
return null;
	}
	public ArrayList<User> readSelectedUsers(int id){
		ArrayList<User> list=new ArrayList<User>();
		try {
			PreparedStatement ps=con.prepareStatement("select * from users where id=?");
			ps.setInt(1, id);
			ResultSet rs=ps.executeQuery();
		while(rs.next()) {
			User user=new User();
			user.setId(rs.getInt(1));
			user.setName(rs.getString(2));
			user.setPassword(rs.getString(3));
			user.setAge(rs.getInt(4));
			list.add(user);
		}
		}catch(Exception e) {
			e.printStackTrace();
		}
		return list;
	}
	public ArrayList<User> searchSelectedUsers(String name){
		ArrayList<User> list=new ArrayList<User>();
		try {
			PreparedStatement ps=con.prepareStatement("select * from users where name like ? ");
			ps.setString(1, "%" + name +"%");
			ResultSet rs=ps.executeQuery();
			
		while(rs.next()) {
			User user=new User();
			user.setId(rs.getInt(1));
			user.setName(rs.getString(2));
			user.setPassword(rs.getString(3));
			user.setAge(rs.getInt(4));
			list.add(user);
		}
		}catch(Exception e) {
			e.printStackTrace();
		}
		return list;
	}


}
