package com.demo.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class UpdateServlet
 */
@WebServlet("/UpdateServlet")
public class UpdateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	 Connection con;
	    public void init() {
	    	try {
	    		Class.forName("oracle.jdbc.driver.OracleDriver");
	    		 con=DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:oracle","hr","hr");
	    	}catch(Exception e) {
	    		e.printStackTrace();
	    	}
	    }
	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UpdateServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		PrintWriter out=response.getWriter();
		response.setContentType("text/html");
		int idval=Integer.parseInt(request.getParameter("i"));
		int id=Integer.parseInt(request.getParameter("id"));
				String name=request.getParameter("name");
				String password=request.getParameter("password");
				
			int age=Integer.parseInt(request.getParameter("age"));
	
			int num=0;
				try {
					PreparedStatement ps=con.prepareStatement("update users set id=?, name=?, password=?, age=? where id=?" );
				ps.setInt(1, id);
				ps.setString(2, name);
				ps.setString(3, password);
				ps.setInt(4, age);
				ps.setInt(5, idval);
				num=ps.executeUpdate();
				if(num>0) {
					response.sendRedirect("viewAll.jsp");
				}
				}catch(Exception e) {
					e.printStackTrace();
				}
	}

}
